# FoodVentory

By: Maya Rubins

---slide---

## Project Overview

---slide---

#### The Product

<p> FoodVentory is an app developed to help take stock, manage, and order inventory on food trucks throughout the United States.</p>

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/foodventoryhomepage.png" width="35%">

---slide---

#### Project Duration

<p>November 2021 - April 2022</p>

---slide---


#### The Problem

<p> Busy food truck workers and owners lack the time necessary to order and manage their inventory.</p> 

---slide---

#### The Goal

<p> Design an app that allows the user to order and manage their inventory.</p>

---slide---

#### My Role

<p> I was the lead UX designer and UX researcher for this project.</p>

---slide---

#### My Responsibilities
* Conducted user interviews
* Created paper and digital wireframes
* Developed low and high-fidelity prototypes
* Conducted usability studies

---slide---

## Understanding the User
***
* User Research
* Personas
* Problem Statements
* User Journey Maps

---slide---

## User Research

---slide---

#### Summary

<p>I conducted user interviews and created empathy maps to better understand the users I was designing for and their needs.</p>

---slide---

#### Primary User Group

<p>Food truck owners who don't have time to keep track of their inventory.</p>

---slide---

#### Results

<p>The user research revealed that managing inventory, updating menus, storing food, using food, and receving food were all limiting factors for food truck owners.</p>

---slide---

#### User Pain Points

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/painpoints.png" width="100%">

---slide---

## Personas

---slide---

#### Meet Dmitry

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/dmitry.png" width="100%">

---slide---

## Problem Statement

---slide---

#### Dmitry's Problem

<p> Dmitry is a food truck owner who needs help managing his inventory because he wants to spend more time cooking insead of counting.</p>

---slide---

## User Journey Map

---slide---

#### Dmitry's Journey Map

<p> Mapping Dmitry's journey revealed how helpful it would be for users to coordinate orders for a food truck while also maintaining a busy schedule.</p>

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/dmitrychart.png" width="100%">

---slide---

## Starting the Design
***
* Paper Wireframes
* Digital Wireframes
* Low-fidelity Prototypes
* Usability Studies

---slide---

## Paper Wireframes

---slide---

#### Wireframing Process

<p> Drafting iterations of the app on paper ensured that the elements that made it to digital wireframes would be able to address the users pain points.</p>

---slide---

#### Initial Wireframes

<p>Stars were used to mark the elements of each sketch that would be used in the digital wireframes.</p>

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/foodwireframe.png" width="30%">

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/foodwireframe1.png" width="30%">

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/foodwireframe2.png" width="30%">

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/foodwireframe3.png" width="30%">

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/foodwireframe4.png" width="30%">

---slide---

#### Finalized Wireframes

<p> For the final draft of the home screen, I prioritized a <strong>quick and easy ordering process</strong> to help users save time.</p>

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/foodwireframe5.png" width="30%">

---slide---

## Digital Wireframes

---slide---

#### Inital Digital Wireframes

<p> As the initial design phase continued, I made sure to create the screen designs based on feedback and findings from the user research.</p>

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/digitalfoodwire.png" width="75%">

---slide---

#### Finalized Digital Wireframes

<p>When selecting a product to place in the users cart, the design allows for accuracy through images and informational text.</p>

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/digitalfoodwire1.png" width="80%">

---slide---

## Low-Fidelity Prototype

---slide---

#### Summary

<p>Using the completed set of digital wireframes, I created a low-fidelity prototype. The primary user flow was to create a new FoodVentory acccount, login and create a new order, so that the prototype could be used in a usability study.</p>

---slide---

<video width="38%" data-autoplay src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/low-fidelity.mov"></video>

---slide---

## Usability Study

---slide---

#### Findings

<p> I conducted two rounds of usability studies. Findings from the first study helped me guide the designs from wireframes to mockups. The second study used a high-fidelity prototype and revealed what aspects of the mockup needed refining.</p>

---slide---

#### Round 1 Findings
***
* Users want to be able to create their own profile.
* Users want to easily navigate between pages.
* Users want to save their payment method for a faster checkout.

---slide---

#### Round 2 Findings
***
* Users want to be able to select a quantity of items.
* Users want to click on a drop-down to add information.
* Users want a keyboard to pop up when they click in a text field.

---slide---

## Refining the Design
***
* Mockups
* High-fidelity Prototypes
* Accessibility

---slide---

## Mockups

---slide---

#### Round 1 Mockups

<p> Early designs allowed for some customization, after the usability studies I added options for the user to <strong> select a quantity and see all of the customization options.</strong></p>

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/before.png" width="35%">

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/after.png" width="35%">

---slide---

#### Round 2 Mockups

<p> After the second usability study I ended up changing the color pallet to improve accessibility. I also updated the <strong>calendar and time function on the "schedule delivery" page</strong> so users can now update the time and date.<p>

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/before1.png" width="35%">

---slide---

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/after1.png" width="35%">

---slide---

#### Finalized Mockups

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/foodscreens.png" width="100%">

---slide---

## High-fidelity Prototype

---slide---

#### Summary

<p> The final high-fidelity prototype presented cleaner user flows for creating a new order. It also met user needs for selecting a date and time, along with adding more customization and updated color pallet for accessibility.</p>

---slide---

<video width="38%" data-autoplay src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/high-fidelity.mov"></video>


---slide---

## Accessibility Considerations

---slide---

#### Conserns to Consider

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/accessibility.png" width="100%">


---slide---

## Going Forward
***
* Take Aways
* Next Steps

---slide---

## Take Aways

---slide---

#### Impact

<p> The designs really impacted how the user can shop for and manage thier inventory effectively while saving time.</p>

---slide---

#### Quote from User Feedback

_"I love how easy this app is to use. I was able to place my inventory order in minutes."_

---slide---

#### What I Learnt

<p>The first ideas for the app are only the beginning of the process. Usability studies and user feedback influence each and every iteration of the apps design.</p>

---slide---

## Next Steps

---slide---

#### The Future of FoodVentory

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/nextsteps.png" width="100%">

---slide---

## Let's Connect!

---slide---

#### Stay in Touch

<p> Thank you for reviewing my work on the FoodVentory app! If you'd like to see more or get in touch, my contact information is provided on the next page.</p>

---slide---

<section data-auto-animate>

#### Reach Out

<div data-id="maya">

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/maya1.jpg" style="width:50%;">
</div>
</section>

---slide---

<section data-auto-animate>

#### Reach Out

<div data-id="maya">

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/maya1.jpg" style="width:40%;">
</div>

<div data-id="contact">

<a href="mailto: maya.rubins@gmail.com">Send Email</a>
</div>
</section>

---slide---

##### Reach Out

<section data-auto-animate>

<div data-id="maya">
<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/maya1.jpg" style="width:30%;">
</div>
<div data-id="contact">

<a href="mailto: maya.rubins@gmail.com">Send Email</a>

<a href="https://mrubins05.gitlab.io/portfolio/index.html">My Website</a>
</div>
</section>

---slide---

#### Reach Out

<section data-auto-animate>

<div data-id="maya">
<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/maya1.jpg" style="width:20%;">
</div>

<div data-id="contact">

<a href="mailto: maya.rubins@gmail.com">Send Email</a>

<a href="https://mrubins05.gitlab.io/portfolio/index.html">My Website</a>

<a href="https://www.figma.com/file/uLJu9eH8BS0zUJc348nNs4/FoodVentory-App?node-id=77%3A136">FoodVentory App</a>
</div>
</section>

---slide---

# Thank You!
***

 <a href="https://mrubins05.gitlab.io/portfolio/index.html"><p>Back to my Portfolio :)</p>
 </a>

